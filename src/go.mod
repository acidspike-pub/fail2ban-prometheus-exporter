module gitlab.com/hectorjsmith/fail2ban-prometheus-exporter

go 1.15

require (
	github.com/alecthomas/kingpin/v2 v2.3.2
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/kisielk/og-rek v1.2.0
	github.com/nlpodyssey/gopickle v0.2.0
	github.com/prometheus/client_golang v1.14.0
	github.com/prometheus/common v0.42.0 // indirect
	github.com/prometheus/procfs v0.9.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	google.golang.org/protobuf v1.30.0 // indirect
)
